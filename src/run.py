from __future__ import annotations

import base64
import time
from datetime import datetime
from io import BytesIO

from flask import Flask
from matplotlib.figure import Figure

app = Flask(__name__)


@app.route('/')
def getweather(api_key=None):
    import json
    import requests
    city, lat, lon = 'Saint Petersburg, RU', 59.57, 30.19
    api_key = '26f1cc6b039373fdded0b5855a718b65'
    dt = int(time.time())

    if api_key:
        """
        req = requests.get(
            f'http://api.openweathermap.org/data/2.5/'
            f'onecall/timemachine?lat={lat}&lon={lon}&'
            f'appid={api_key}&type=day&cnt=5')
        """
        req = requests.get('http://api.openweathermap.org/data/2.5/forecast',
                           params={'lat': lat, 'lon': lon, 'units': 'metric', 'lang': 'ru', 'APPID': api_key})

        data = req.json()
        dates, temps = [], []
        for i in data['list']:
            temps.append(int('{:+3.0f}'.format(i['main']['temp'])))
            dates.append(
                datetime.utcfromtimestamp(
                    i['dt']).strftime('%d/%m %H'))

        date, temp, avg_temp, avg_date = [], [], [], []

        for i in range(0, len(temps), 8):
            sum = 0
            temp.append(temps[i])
            date.append(dates[i])
            for j in range(8):
                sum = sum + temps[i + j]

            avg_temp.append(sum / 8)
            avg_date.append(dates[i])

        fig = Figure()
        axs = fig.subplots(1, 2)
        fig.tight_layout(pad=2)
        fig.set_size_inches(8, 4)

        axs[0].scatter(date, temp)
        axs[1].plot(avg_date, avg_temp)

        axs[0].set_xlabel('Дата')
        axs[0].set_ylabel('Температура')
        axs[0].set_title('Погода за 5 дней')

        axs[1].set_xlabel('Дата')
        axs[1].set_ylabel('Температура')
        axs[1].set_title('Погода-среднее значение')

        buf = BytesIO()
        fig.savefig(buf, format='png')

        data = base64.b64encode(buf.getbuffer()).decode('ascii')
        return f"<img src='data:image/png;base64,{data}'/>"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
